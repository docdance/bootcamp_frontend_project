import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Vender } from '@/types/Vender'
import { useLoadingStore } from './loading'
import venderService from '@/services/vender'
import { useMessageStore } from './message'

export const useVenderStore = defineStore('vender', () => {
  const venders = ref<Vender[]>([])
  const loadingStore = useLoadingStore()
  // const messageStore = useMessageStore()
  const showVender = ref(false)
  const lastId = ref(5)
  const currentVender = ref<Vender | null>()
  const isVender = ref(false)
  currentVender.value = null

  // const initialVender: Vender = {
  //   name: '',
  //   tel: ''
  // }
  // const editedVender = ref<Vender>(JSON.parse(JSON.stringify(initialVender)))

  function getVenders() {
    venderService.getVenders()
  }

  const searchVender = async (name: string) => {
    loadingStore.doLoading()
    const res = await venderService.getVenderByName(name)
    currentVender.value = res.data
    loadingStore.finish()
    isVender.value = true
  }

  // async function addVender(name: string, tel: string) {
  //   try {
  //     loadingStore.doLoading()
  //     editedVender.value.name = name
  //     editedVender.value.tel = tel
  //     const vender = editedVender.value

  //     console.log('Post ' + JSON.stringify(vender))
  //     const res = await venderService.addVender(vender)

  //     await getVenderByName(name)
  //     loadingStore.finish()
  //   } catch (e: any) {
  //     messageStore.showMessage(e.message)
  //     loadingStore.finish()
  //   }

  function addVender(name: string, tel: string) {
    const newVender = ref<Vender>({
      id: lastId.value++,
      name: name,
      tel: tel
    })
    venderService.addVender(newVender.value)
  }

  // const getVenderByTel = (tel: string): Vender | null => {
  //   for (const vender of venders.value) {
  //     if (vender.tel === tel) return vender
  //   }
  //   return null
  // }

  // const getVenderByName = (name: string): Vender | null => {
  //   for (const vender of venders.value) {
  //     if (vender.name === name) return vender
  //   }
  //   return null
  // }

  // const searchVender = (name: string) => {
  //   const index = venders.value.findIndex((item) => item.name === name)
  //   if (index < 0) {
  //     currentVender.value = null
  //   }
  //   currentVender.value = venders.value[index]
  // }

  function clear() {
    showVender.value = false
    isVender.value = false
    currentVender.value = null
  }

  return {
    venders,
    showVender,
    getVenders,
    addVender,
    clear,
    searchVender,
    currentVender,
    isVender
  }
})
