import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'
import authService from '@/services/auth'
import { useMessageStore } from './message'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import type { Member } from '@/types/Member'
export const useAuthStore = defineStore('auth', () => {
  const messageSttore = useMessageStore()
  const router = useRouter()
  const loadingStore = useLoadingStore()
  const member = ref(false)
  const user = ref(false)
  const manager = ref(false)
  const owner = ref(false)

  const login = async function (email: string, password: string) {
    loadingStore.doLoading()

    try {
      const res = await authService.login(email, password)
      console.log(res.data)
      if (res.data.member) {
        member.value = true

        localStorage.setItem('member', JSON.stringify(res.data.member))
        localStorage.setItem('access_token', res.data.access_token)
        messageSttore.showMessage('Member Login Success')
        console.log('Member Login Success')
        router.replace('/member')
        // replace back ไม่ได้ push back ได้
      } else {
        if (res.data.user.roles.find((role: { name: string }) => role.name === 'owner')) {
          owner.value = true
        } else if (res.data.user.roles.find((role: { name: string }) => role.name === 'manager')) {
          manager.value = true
        } else {
          user.value = true
        }

        localStorage.setItem('user', JSON.stringify(res.data.user))
        localStorage.setItem('access_token', res.data.access_token)
        messageSttore.showMessage('User Login Success')
        console.log('User Login Success')
        // replace back ไม่ได้ push back ได้
        router.replace('/pos')
      }
    } catch (e: any) {
      console.log(e)
      messageSttore.showMessage(e.message)
    }
    loadingStore.finish()
  }

  const logout = async function () {
    await router.replace('/login')
    localStorage.removeItem('user')
    localStorage.removeItem('member')
    localStorage.removeItem('access_token')
    member.value = false
    user.value = false
    manager.value = false
    owner.value = false
  }

  function getCurrentUser(): User | null {
    const strUser = localStorage.getItem('user')
    if (strUser === null) return null
    const user1: User = JSON.parse(strUser)
    if (user1.roles.find((role: { name: string }) => role.name === 'user')) user.value = true
    if (user1.roles.find((role: { name: string }) => role.name === 'manager')) manager.value = true
    if (user1.roles.find((role: { name: string }) => role.name === 'owner')) owner.value = true

    return user1
  }

  function getCurrentMember(): Member | null {
    const strMember = localStorage.getItem('member')
    if (strMember === null) return null
    member.value = true
    return JSON.parse(strMember)
  }

  function getToken(): string | null {
    const strToken = localStorage.getItem('access_token')
    if (strToken === null) return null
    return JSON.parse(strToken)
  }
  return { getCurrentUser, getCurrentMember, login, getToken, logout, member, user, manager, owner }
})
