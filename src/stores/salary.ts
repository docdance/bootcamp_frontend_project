import type { Salary } from '@/types/Salary'
import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { onMounted } from 'vue'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { id } from 'vuetify/locale'
import type { VForm } from 'vuetify/components'

import salaryService from '@/services/salary'

export const useSalaryStore = defineStore('salary', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const form = ref(false)
  const refForm = ref<VForm | null>(null)
  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)

  function getCurrentDate(): Date {
    return new Date() // Returns the current date and time
  }

  const initialSalary: Salary = {
    id: 0,
    userId: 0,
    date: getCurrentDate(),
    pay: 0,
    salaryStatus: 'unpaid'
  }

  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initialSalary)))
  const salarys = ref<Salary[]>([])

  async function getSalary(id: number) {
    loadingStore.doLoading()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }

  async function getSalarys() {
    try {
      loadingStore.doLoading()
      const res = await salaryService.getSalarys()
      salarys.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }

  function formatDate(dateString: string): string {
    const dateObject = new Date(dateString)
    const year = dateObject.getFullYear()
    const month = (1 + dateObject.getMonth()).toString().padStart(2, '0')
    const day = dateObject.getDate().toString().padStart(2, '0')
    return `${year}-${month}-${day}`
  }

  async function saveSalary() {
    try {
      loadingStore.doLoading()
      const salary = editedSalary.value
      if (!salary.id) {
        // Add new
        console.log('Post ' + JSON.stringify(salary))
        // Create a new Date object from the date string
        salary.date = new Date(salary.date)
        const res = await salaryService.addSalary(salary)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(salary))
        const res = await salaryService.updateSalary(salary)
      }

      await getSalarys()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function deleteSalary() {
    loadingStore.doLoading()
    const salary = editedSalary.value
    const res = await salaryService.delSalary(salary)

    await getSalarys()
    loadingStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initialSalary))
  }

  function getColor(salaryStatus: string) {
    if (salaryStatus === 'paid') return 'green'
    else return 'orange'
  }

  async function showdetail(id: number) {
    try {
      loadingStore.doLoading()
      const res = await salaryService.getSalary(id)
      editedSalary.value = res.data

      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  return {
    dialog,
    salarys,
    getSalarys,
    saveSalary,
    deleteSalary,
    editedSalary,
    getSalary,
    clearForm,
    getColor,
    showdetail
  }
})
