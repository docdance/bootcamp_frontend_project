import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from '../loading'
import rerportService from '@/services/stocks/report'
import axios from 'axios'

export const useReportStore = defineStore('report', () => {
  const loadingStore = useLoadingStore()
  const years = ref([])
  const year1 = ref('')
  const month1 = ref('')
  const m = ref('')
  const day1 = ref('')
  const dialogGraph = ref(false)
  const graphQtyStock = ref(false)

  function openGraph() {
    dialogGraph.value = true
  }

  function openGraph2() {
    graphQtyStock.value = true
  }

  function closeGraph() {
    dialogGraph.value = false
    graphQtyStock.value = false
  }

  const chartData = ref({
    labels: [],
    datasets: [
      {
        label: ' ',
        backgroundColor: '#d3ad79',
        data: []
      }
    ]
  })

  const chartOptions = ref({
    responsive: true
  })

  async function getYears() {
    try {
      loadingStore.doLoading()
      const res = await rerportService.getYears()
      years.value = res.data

      // res = await productService.getProducts()
      // products2.value = res.data
      console.log(years)

      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  async function getReportUsedYear(year: string) {
    year1.value = year
    console.log(year1.value)
    const res = await axios.get(`http://localhost:3000/stock-report/reportUsed/${year1.value}`)
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.used * -1
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' Stock Used',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    year1.value = ''
  }

  async function getReportUsedMonth(year: string, month: string) {
    year1.value = year
    getMonth(month)

    console.log('111111111' + m.value + ' ' + year1.value)
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportUsed/${year1.value}/${m.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.used * -1
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' Stock Used',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
  }

  function getItemText(item: string) {
    console.log('Selected Year: ' + item)
    year1.value = item
  }

  function getMonth(item: string) {
    console.log('Selected Month: ' + item)
    if (item === 'January') {
      m.value = '01'
    }
    if (item === 'February') {
      m.value = '02'
    }
    if (item === 'March') {
      m.value = '03'
    }
    if (item === 'April') {
      m.value = '04'
    }
    if (item === 'May') {
      m.value = '05'
    }
    if (item === 'June') {
      m.value = '06'
    }
    if (item === 'July') {
      m.value = '07'
    }
    if (item === 'August') {
      m.value = '08'
    }
    if (item === 'September') {
      m.value = '09'
    }
    if (item === 'October') {
      m.value = '10'
    }
    if (item === 'November') {
      m.value = '11'
    }
    if (item === 'December') {
      m.value = '12'
    }
    console.log(month1)
  }

  async function getReportQtyYear(year: string) {
    year1.value = year
    console.log(year1.value)
    const res = await axios.get(`http://localhost:3000/stock-report/reportQty/${year1.value}`)
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.quantity
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' Stock Quantity',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    year1.value = ''
  }

  async function getReportQtyMonth(year: string, month: string) {
    year1.value = year
    getMonth(month)

    console.log('111111111' + m.value + ' ' + year1.value)
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportQty/${year1.value}/${m.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.quantity
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' Stock Quantity',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
  }

  //Purchase Total Price
  async function getReportPurPriceYear(year: string) {
    year1.value = year
    console.log(year1.value)
    const res = await axios.get(`http://localhost:3000/stock-report/reportPurPrice/${year1.value}`)
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' Purchasse Price',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    year1.value = ''
  }
  async function getReportPurPriceMonth(year: string, month: string) {
    year1.value = year
    getMonth(month)

    console.log('111111111' + m.value + ' ' + year1.value)
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportPurPrice/${year1.value}/${m.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' Purchasse Price',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
  }
  async function getReportPurPriceDay(year: string, month: string, day: string) {
    year1.value = year
    m.value = month
    day1.value = day
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportPurPrice/${year1.value}/${m.value}/${day1.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' ' + day1.value + ' Purchase Price',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
    day1.value = ''
  }

  //Purchase Total Quantity
  async function getReportPurQtyYear(year: string) {
    year1.value = year
    console.log(year1.value)
    const res = await axios.get(`http://localhost:3000/stock-report/reportPurQty/${year1.value}`)
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' Purchasse',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    year1.value = ''
  }
  async function getReportPurQtyMonth(year: string, month: string) {
    year1.value = year
    getMonth(month)

    console.log('111111111' + m.value + ' ' + year1.value)
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportPurQty/${year1.value}/${m.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' Purchase Price',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
  }
  async function getReportPurQtyDay(year: string, month: string, day: string) {
    year1.value = year
    m.value = month
    day1.value = day
    const res = await axios.get(
      `http://localhost:3000/stock-report/reportPurQty/${year1.value}/${m.value}/${day1.value}`
    )
    console.log(res)
    const labels = res.data.map((it: any) => {
      return it.name
    })
    const data = res.data.map((it: any) => {
      return it.price
    })
    console.log(labels, data)
    chartData.value = {
      labels: labels,
      datasets: [
        {
          label: year1.value + ' ' + month1.value + ' ' + day1.value + ' Purchasse',
          backgroundColor: '#d3ad79',
          data: data
        }
      ]
    }
    m.value = ''
    month1.value = ''
    year1.value = ''
    day1.value = ''
  }

  function getDay(item: string) {
    day1.value = item
  }

  return {
    graphQtyStock,
    openGraph2,
    openGraph,
    closeGraph,
    dialogGraph,
    years,
    month1,
    getYears,
    year1,
    chartData,
    chartOptions,
    getReportUsedYear,
    getItemText,
    getReportUsedMonth,
    m,
    getMonth,
    getReportQtyYear,
    getReportQtyMonth,
    day1,
    getReportPurPriceYear,
    getReportPurPriceMonth,
    getReportPurPriceDay,
    getReportPurQtyDay,
    getReportPurQtyMonth,
    getReportPurQtyYear,
    getDay
  }
})
