import type { Material } from '@/types/stock/Materials'
import { defineStore } from 'pinia'
import { useLoadingStore } from '../loading'
import { computed, ref } from 'vue'
import materialService from '@/services/stocks/material'
import material from '@/services/stocks/material'
import type { CheckDetail } from '@/types/stock/CheckDetail'
import { useCheckStockStore } from './checkStock'

export const useMaterialStore = defineStore('material', () => {
  const loadingStore = useLoadingStore()
  const checkStockStore = useCheckStockStore()
  const checkStock = ref(false)
  const materials = ref<Material[]>([])
  const editedMaterials = ref<Material[]>([])

  const searchQuery = ref<string>('')
  const qt_previous = ref([0])
  const initialMaterial: Material = {
    name: '',
    in_date: '',
    price: 0,
    qt_previous: 0,
    quantity: 0,
    min: 0,
    use: 0,
    status: 'Low'
  }
  const editedMaterial = ref<Material & { files: File[] }>(
    JSON.parse(JSON.stringify(initialMaterial))
  )
  const initiaStockDetail: CheckDetail = {
    name: editedMaterial.value.name,
    in_date: editedMaterial.value.in_date,
    price: editedMaterial.value.price,
    qt_previous: editedMaterial.value.quantity,
    quantity: 0,
    min: editedMaterial.value.min,
    use: 0,
    status: 'Low',
    materialId: editedMaterial.value.id!,
    material: editedMaterial.value
  }

  const editedStockDetail = ref<CheckDetail>(JSON.parse(JSON.stringify(initiaStockDetail)))

  async function getMaterials() {
    try {
      loadingStore.doLoading()
      const res = await materialService.getMaterials()
      materials.value = res.data

      loadingStore.finish()
    } catch (e) {
      console.log('Error')

      loadingStore.finish()
    }
  }

  async function getMaterial(id: number) {
    try {
      loadingStore.doLoading()
      const res = await materialService.getMaterial(id)
      editedMaterial.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
      // messageStore.showMessage(e.message)
    }
  }

  async function checkAddUpdate(material: Material) {
    if (!material.id) {
      // add new
      // console.log('post => add ' + JSON.stringify(material))
      const res = await materialService.addMaterial(material)
    } else {
      // update
      // console.log('patch => update ' + JSON.stringify(material))
      const res = await materialService.updateMaterial(material)
    }
  }

  function checkStockReport(material: Material) {
    const checkStockReportDetail: CheckDetail = {
      name: material.name,
      in_date: material.in_date,
      price: material.price,
      qt_previous: material.quantity,
      quantity: parseInt(material.quantity.toString()),
      min: material.min,
      use: material.use,
      status: material.status,
      materialId: material.id,
      material: material
    }
    checkStockStore.addCheckStockDetail(checkStockReportDetail)
  }

  async function saveMaterial() {
    try {
      loadingStore.doLoading()
      if (checkStock.value === true) {
        for (let i = 0; i < editedMaterials.value.length; i++) {
          console.log('Pre QTY', qt_previous.value[i])
          editedMaterials.value[i].qt_previous = qt_previous.value[i]

          checkAddUpdate(editedMaterials.value[i])
          checkStockReport(editedMaterials.value[i])
        }
        checkStockStore.saveCheckStock(
          checkStockStore.checkDetails,
          checkStockStore.checkStockDetail
        )
      } else {
        const material = editedMaterial.value
        // console.log('editedMaterial.value ' + JSON.stringify(editedMaterial.value))
        checkAddUpdate(material)
      }

      await getMaterials()
      loadingStore.finish()
    } catch (e: any) {
      //  messageStore.showMessage(e.message)
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function deleteMaterial() {
    loadingStore.doLoading()
    const material = editedMaterial.value
    const res = await materialService.delMaterial(material)

    await getMaterials()
    loadingStore.finish()
  }

  function clearForm() {
    editedMaterial.value = JSON.parse(JSON.stringify(initialMaterial))
  }

  function getColor(status: string) {
    if (status === 'Low') return 'orange'
    else return 'green'
  }

  const filteredMaterials = computed(() => {
    const query = searchQuery.value.toLowerCase().trim()
    if (!query) {
      return materials.value // Return all materials if search query is empty
    } else {
      return materials.value.filter(
        (material) =>
          material.name.toLowerCase().includes(query) || material.price.toString().includes(query)
        // Add more fields to search as needed
      )
    }
  })
  // const filteredStock = computed(() => {
  //   const query = searchQuery.value.toLowerCase().trim()
  //   if (!query) {
  //     return stock.value // Return all materials if search query is empty
  //   } else {
  //     return stock.value.filter(
  //       (stock) =>
  //         stock.name.toLowerCase().includes(query) || stock.price.toString().includes(query)
  //       // Add more fields to search as needed
  //     )
  //   }
  // })
  return {
    materials,
    initialMaterial,
    editedMaterial,
    editedStockDetail,
    getMaterials,
    saveMaterial,
    deleteMaterial,
    getMaterial,
    clearForm,
    getColor,
    filteredMaterials,
    searchQuery,
    checkStock,
    editedMaterials,
    qt_previous
  }
})
