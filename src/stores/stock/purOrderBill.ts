import { ref } from 'vue'
import { defineStore } from 'pinia'

import { useAuthStore } from '../auth'

import { useVenderStore } from '../vender'
import { usePurOrderStore } from './purOrder'
import type { PurBillDetail } from '@/types/stock/PurOrderDetail'
import type { PurOrderBill } from '@/types/stock/PurOrderBill'
import type { PurOrder } from '@/types/stock/PurOrder'
import type { Material } from '@/types/stock/Materials'

export const useBillOrderStore = defineStore('billPurOrder', () => {
  const authStore = useAuthStore()

  const venderStore = useVenderStore()
  const purOrderStore = usePurOrderStore()
  const importOrder = ref(false)
  const billDialog = ref(false)
  const billPayment = ref(false)
  const qrCode = ref(false)
  // const cash = ref(0)

  const billItems = ref<PurBillDetail[]>([])
  const billPurOrder = ref<PurOrderBill>({
    id: -1,
    total: 0,
    quantity: 0,
    date: new Date().toString(),
    purBillDetails: [],
    user: authStore.getCurrentUser(),
    nameStore: 'coffee',
    vender: null
  })

  function addPurBills() {
    //////////////// SAVE RECEIPT ///////////////////
    billPurOrder.value.purBillDetails = billItems.value
    // billPurOrder.value.cash = cash.value
    if (venderStore.currentVender) {
      billPurOrder.value.vender = venderStore.currentVender
    }

    const purOrder: PurOrder = {
      purOrderDetails: [],
      userId: authStore.getCurrentUser()?.id ?? null,
      date: billPurOrder.value.date,
      total: billPurOrder.value.total,
      quantity: billPurOrder.value.quantity,
      nameStore: billPurOrder.value.nameStore,
      vender: billPurOrder.value.vender,
      status: '',
      orderDetails: []
    }

    for (let i = 0; i < billItems.value.length; i++) {
      const newOrderItem = {
        materialId: billItems.value[i].material.id!,
        quantity: billItems.value[i].quantity
      }
      purOrder.purOrderDetails.push(newOrderItem)
    }

    purOrderStore.savePurOrder(purOrder)
  }

  ////////////////// Receipt Items //////////////////////////
  const incUnitOfBillItem = (selectedItem: PurBillDetail) => {
    selectedItem.quantity++
  }
  function addBillItem(material: Material) {
    const index = billItems.value.findIndex((item) => item.material.id === material.id)
    if (index >= 0) {
      billItems.value[index].quantity++
      billItems.value[index].price + material.price
    } else {
      const newBillOrder: PurBillDetail = {
        id: 1,
        name: material.name,
        price: material.price,
        quantity: 1,
        total: material.price * material.quantity,
        materialId: material.id!,
        material: material
      }
      billItems.value.push(newBillOrder)
    }

    calBill()
  }

  // function updateSweetness(item: PurOrderDetail, sweetnessLevel: string): void {
  //   item.sweet = sweetnessLevel
  // }

  ////////////////// Calculate Receipt //////////////////////////
  function calBill() {
    let totalBefore = 0
    // let promotionDiscount = 0
    billPurOrder.value.quantity = 0
    //Cal totalBefor
    for (const item of billItems.value) {
      totalBefore = totalBefore + item.material.price * item.quantity
      billPurOrder.value.quantity += item.quantity
    }

    // promotionDiscount += calPromotions(promotionDiscount, totalBefore)

    // receipt.value.discount = promotionDiscount

    // receipt.value.total = totalBefore - promotionDiscount
    billPurOrder.value.total = totalBefore
  }

  ////////// function calPromotions /////////////
  // function calPromotions(promotionDiscount: number, totalBefore: number) {
  //   //// Check PromotionSelected
  //   for (const item of receiptPromotion.promotionSelected) {
  //     /// Check IsNotforOneItem?
  //     if (item.forOneItem === false) {
  //       // Check IsPercentDiscount?
  //       if (item.percentDiscount > 0) {
  //         // PercentDiscount
  //         promotionDiscount += (totalBefore * item.percentDiscount) / 100
  //       } else if (item.priceDiscount > 0) {
  //         // PriceDiscount
  //         promotionDiscount += item.priceDiscount
  //       }
  //     }

  //     /// Check IsforOneItem?
  //     if (
  //       item.forOneItem === true &&
  //       (item.minQty > 0 || item.minPrice > 0) &&
  //       item.productId >= 0
  //     ) {
  //       // console.log('Total before3: ', totalBefore)
  //       const receiptItemIndex = billItems.value.findIndex(
  //         (itemP) => itemP.product.id === item.productId
  //       )

  //       let qty = 0 // qty of product not qty sweetness
  //       for (let i = 0; i < billItems.value.length; i++) {
  //         if (billItems.value[i].productId === item.productId) {
  //           qty += billItems.value[i].qty
  //         }
  //       }

  //       if (receiptItemIndex !== -1) {
  //         const qtyDiscount = Math.floor(qty / item.minQty)
  //         if (item.percentDiscount > 0) {
  //           // console.log(receiptItems.value[receiptItemIndex].qty, item.minQty, qtyDiscount)
  //           // for loop for increase promotion by qty condition
  //           for (let i = 0; i < qtyDiscount; i++) {
  //             promotionDiscount +=
  //               ((billItems.value[receiptItemIndex].product.price * item.percentDiscount) / 100) *
  //               item.minQty
  //           }

  //           // console.log('qtyDiscount', qtyDiscount)
  //         } else if (item.priceDiscount > 0) {
  //           promotionDiscount += item.priceDiscount * qtyDiscount
  //         }
  //       }
  //     }

  //     if (memberStore.currentMember && item.member) {
  //       if (item.getPoint > 0) {
  //         gotPoint.value += item.getPoint
  //       }
  //       if (item.usePoint > 0) {
  //         usePoint.value += item.usePoint
  //       }
  //     }
  //   }
  //   return promotionDiscount
  // }

  //////////////////////// Remove Items ////////////////////////////
  function removeBillItem(billItem: PurBillDetail) {
    const index = billItems.value.findIndex((item) => item === billItem)
    billItems.value.splice(index, 1)
    calBill()
  }
  function increase(item: PurBillDetail) {
    item.quantity++
    calBill()
  }
  function decrease(item: PurBillDetail) {
    if (item.quantity === 1) {
      removeBillItem(item)
    }
    item.quantity--
    calBill()
  }

  // function showBillPayment() {
  //   if (billItems.value.length > 0) {
  //     billPayment.value = true
  //   }
  // }

  function showBillDialog() {
    billPurOrder.value.purBillDetails = billItems.value
    // console.log(receipt.value.receiptItems)
    billDialog.value = true
  }

  //////////////////////// Clear Receipt ////////////////////////////
  function clear() {
    importOrder.value = false
    billDialog.value = false
    qrCode.value = false
    billItems.value = []
    billPurOrder.value = {
      total: 0,
      quantity: 0,
      date: new Date().toString(),
      purBillDetails: [],
      user: authStore.getCurrentUser(),
      nameStore: 'coffee',
      vender: null
    }
    venderStore.clear()
    // receiptPromotion.clearPromotionSelect()
  }

  function clearForm() {
    billDialog.value = false
    billItems.value = []
    billPurOrder.value = {
      total: 0,
      quantity: 0,
      date: new Date().toString(),
      purBillDetails: [],
      user: authStore.getCurrentUser(),
      nameStore: 'coffee',
      vender: null
    }
    venderStore.clear()
  }

  function showImport() {
    importOrder.value = true
  }

  function showPurBillDialog() {
    addPurBills()
    console.log(billPurOrder)

    // // editedPurOrder.value.orderDetails = orderDetails.value
    // billPurOrder.value.orderDetails = orderDetails.value

    billDialog.value = true
  }

  function getColorStatus(status: string) {
    if (status === 'Low') return '#EF6C00'
    else return '#009688'
  }

  return {
    showImport,
    clearForm,
    getColorStatus,
    billItems,
    addBillItem,
    removeBillItem,
    increase,
    decrease,
    billPurOrder,
    calBill,
    showBillDialog,
    billDialog,
    clear,
    showPurBillDialog,
    importOrder,
    billPayment,
    qrCode,
    addPurBills
  }
})
