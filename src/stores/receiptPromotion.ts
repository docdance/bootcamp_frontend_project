import { nextTick, ref } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/Promotion'

export const useReceiptPromotion = defineStore('receiptPromotion', () => {
  const promotionSelected = ref<Promotion[]>([])
  const textPromotions = ref<string[]>([])
  const renderComponent = ref(true)

  function addPromotionItem(promotion: Promotion) {
    const index = promotionSelected.value.findIndex((item) => item.id === promotion.id)
    if (index >= 0) {
      return
    } else {
      promotionSelected.value.push(promotion)
    }
    textPromotion()
  }

  function textPromotion() {
    textPromotions.value = []
    for (let i = 0; i < promotionSelected.value.length; i++) {
      textPromotions.value.push(promotionSelected.value[i].name)
      console.log(promotionSelected.value[i].name)
    }

    console.log('textPromotion', textPromotions)
  }
  const clearPromotionSelect = async () => {
    promotionSelected.value = []
    textPromotions.value = []
    // Remove MyComponent from the DOM
    renderComponent.value = false

    // Wait for the change to get flushed to the DOM
    await nextTick()

    // Add the component back in
    renderComponent.value = true
  }

  return {
    addPromotionItem,
    promotionSelected,
    clearPromotionSelect,
    renderComponent,
    textPromotions
  }
})
