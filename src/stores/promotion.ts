import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import type { Promotion } from '@/types/Promotion'
import type { PromotionAdd } from '@/types/PromotionAdd'
import promotionService from '@/services/promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const useLoading = useLoadingStore()

  const currentDate = new Date().toISOString().slice(0, 10) // Format: 'YYYY-MM-DD'
  const promotions = ref<Promotion[]>([])
  const promotionsByDate = ref<Promotion[]>([])
  const initialPromotion: Promotion = {
    id: -1,
    name: '',
    description: '',
    start_date: '',
    end_date: '',
    percentDiscount: 0,
    priceDiscount: 0,
    minQty: 0,
    minPrice: 0,
    member: false,
    productId: -1,
    forOneItem: false,
    getPoint: 0,
    usePoint: 0
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))
  const editedIndex = -1
  const startDate = ref<Date>(new Date())
  const endDate = ref<Date>(new Date())

  const refreshEditedPromotion = () => {
    return (editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion)))
  }

  function ConvertStartDate(promotion: Promotion) {
    const [year, month, day] = promotion.start_date.split('-').map(Number)
    startDate.value = new Date(year, month - 1, day)
  }
  function ConvertEndDate(promotion: Promotion) {
    const [year, month, day] = promotion.end_date.split('-').map(Number)
    endDate.value = new Date(year, month - 1, day)
  }

  function setDateToObject(start: Date, end: Date) {
    const year1 = start.getFullYear()
    const month1 = (start.getMonth() + 1).toString().padStart(2, '0') // Add leading zero if month < 10
    const day1 = start.getDate().toString().padStart(2, '0')
    editedPromotion.value.start_date = `${year1}-${month1}-${day1}`

    const year2 = end.getFullYear()
    const month2 = (end.getMonth() + 1).toString().padStart(2, '0') // Add leading zero if month < 10
    const day2 = end.getDate().toString().padStart(2, '0')
    editedPromotion.value.end_date = `${year2}-${month2}-${day2}`
  }

  async function savePromotion(promotion: Promotion) {
    useLoading.doLoading()
    if (promotion.id === -1 || !promotion.id) {
      //convert Type
      const newPromotion: PromotionAdd = {
        name: promotion.name,
        description: promotion.description,
        start_date: promotion.start_date, // You can initialize with a default date if needed
        end_date: promotion.end_date, // You can initialize with a default date if needed
        percentDiscount: parseInt((promotion.percentDiscount ?? 0).toString()),
        priceDiscount: parseInt((promotion.percentDiscount ?? 0).toString()),
        getPoint: parseInt((promotion.getPoint ?? 0).toString()),
        usePoint: parseInt((promotion.usePoint ?? 0).toString()),
        minQty: parseInt((promotion.minQty ?? 0).toString()),
        minPrice: parseInt((promotion.minPrice ?? 0).toString()),
        member: promotion.member,
        productId: parseInt(promotion.productId.toString()),
        forOneItem: promotion.forOneItem
      }

      //add user
      console.log(newPromotion)
      const res = await promotionService.addPromotion(newPromotion)
    } else {
      //update user
      const res = await promotionService.updatePromotion(promotion)
    }
    startDate.value = new Date()
    endDate.value = new Date()
    await getPromotions()
    useLoading.finish()
  }

  async function delPromotion(promotion: Promotion) {
    useLoading.doLoading()
    const res = await promotionService.delPromotion(promotion)
    await getPromotions()
    useLoading.finish()
  }

  async function getPromotions() {
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    getPromotionsByDate()
  }

  async function getPromotion(id: number) {
    useLoading.doLoading()
    const res = await promotionService.getPromotion(id)
    useLoading.finish()
  }

  function getPromotionsByDate() {
    promotionsByDate.value = promotions.value.filter(
      (promotion) => currentDate >= promotion.start_date && currentDate <= promotion.end_date
    )
    // console.log(promotions)
    // console.log(promotionsByDate)
  }

  return {
    promotions,
    initialPromotion,
    editedPromotion,
    editedIndex,
    startDate,
    endDate,
    ConvertStartDate,
    ConvertEndDate,
    getPromotion,
    getPromotions,
    delPromotion,
    savePromotion,
    setDateToObject,
    getPromotionsByDate,
    promotionsByDate,
    refreshEditedPromotion
  }
})
