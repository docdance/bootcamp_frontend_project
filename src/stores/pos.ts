import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import productService from '@/services/product'
import { useMessageStore } from './message'
import type { Product } from '@/types/Product'
import { useProductStore } from './product'
// import { Type } from '@/types/Type'

export const usePosStore = defineStore('counter', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const productStore = useProductStore()

  const arrayOfProductTypes = ref<Array<Product[]>>([])
  const productsType = ref<Product[]>([])
  // const productsType1 = ref<Product[]>([])
  // const productsType2 = ref<Product[]>([])
  // const productsType3 = ref<Product[]>([])

  async function getProducts() {
    try {
      // const resType = await productService.getType()
      // const type.value = resType.data

      // console.log("Types", productStore.type)
      // arrayOfProductTypes.value.pop()
      for (let i = 0; i < productStore.type.length; i++) {
        const res = await productService.getProductByType(productStore.type[i].name)
        productsType.value = res.data
        // console.log("Types", productsType.value)
        arrayOfProductTypes.value.push(productsType.value)
      }
      console.log('ArrayOfProductOfTypesssssssssssssssssss', arrayOfProductTypes.value)

      // const res1 = await productService.getProductByType('Coffee')
      // productsType1.value = res1.data
      // const res2 = await productService.getProductByType('Drink')
      // productsType2.value = res2.data
      // const res3 = await productService.getProductByType('Dessert')
      // productsType3.value = res3.data
    } catch (error: any) {
      messageStore.showMessage(error.message)
      loadingStore.finish()
    }
  }

  return { getProducts, arrayOfProductTypes }
})
