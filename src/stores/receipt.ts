import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/Receipt/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import type { Order } from '@/types/Receipt/Order'
import { useReceiptPromotion } from './receiptPromotion'
import { useOrderStore } from './order'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const orderStore = useOrderStore()
  const receiptPromotion = useReceiptPromotion()
  const receiptDialog = ref(false)
  const receiptPayment = ref(false)
  const qrCode = ref(false)
  const cash = ref(0)
  const gotPoint = ref(0)
  const usePoint = ref(0)
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>({
    total: 0,
    qty: 0,
    created: new Date(),
    receiptItems: [],
    user: authStore.getCurrentUser(),
    cash: 0,
    discount: 0,
    member: null,
    payment: 'cash'
  })

  async function addReceiptBills() {
    //////////////// SAVE RECEIPT ///////////////////
    receipt.value.receiptItems = receiptItems.value
    receipt.value.cash = cash.value
    if (memberStore.currentMember) {
      gotPoint.value += Math.floor(receipt.value.total / 10)
      memberStore.currentMember.point -= usePoint.value
      memberStore.currentMember.point += gotPoint.value
      receipt.value.member = memberStore.currentMember
    }

    const order: Order = {
      orderItems: [],
      userId: authStore.getCurrentUser()?.id ?? null,
      cash: receipt.value.cash,
      discount: receipt.value.discount,
      promotions: receiptPromotion.promotionSelected,
      member: receipt.value.member,
      getPoint: gotPoint.value,
      payment: receipt.value.payment,
      usePoint: usePoint.value,
      orderItemsIdQty: [],
      total: 0,
      qty: 0,
      branch: {
        id: authStore.getCurrentUser()?.branchs.id,
        name: '',
        address: ''
      }
    }

    for (let i = 0; i < receiptItems.value.length; i++) {
      console.log(receiptItems.value[i].sweet)
      const newOrderItem = {
        productId: receiptItems.value[i].product.id!,
        sweet: receiptItems.value[i].sweet,
        qty: receiptItems.value[i].qty
      }
      order.orderItemsIdQty.push(newOrderItem)
    }
    console.log(order)
    await orderStore.saveOrder(order)
  }

  ////////////////// Receipt Items //////////////////////////
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.qty++
  }
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex(
      (item) => item.product.id === product.id && item.sweet === product.sweet
    )
    if (index >= 0) {
      receiptItems.value[index].qty++
      receiptItems.value[index].price + product.price
    } else {
      const newReceipt: ReceiptItem = {
        id: 1,
        name: product.name,
        price: product.price,
        qty: 1,
        sweet: product.sweet,
        productId: product.id!,
        product: product
      }
      receiptItems.value.push(newReceipt)
    }

    calReceipt()
  }

  function updateSweetness(item: ReceiptItem, sweetnessLevel: string): void {
    item.sweet = sweetnessLevel
  }

  ////////////////// Calculate Receipt //////////////////////////
  function calReceipt() {
    console.log('receiptPromotion.promotionSelected', receiptPromotion.promotionSelected)
    gotPoint.value = 0
    usePoint.value = 0
    let totalBefore = 0
    let promotionDiscount = 0
    receipt.value.qty = 0
    //Cal totalBefor
    for (const item of receiptItems.value) {
      totalBefore = totalBefore + item.product.price * item.qty
      receipt.value.qty += item.qty
    }

    promotionDiscount += calPromotions(promotionDiscount, totalBefore)

    receipt.value.discount = promotionDiscount

    receipt.value.total = totalBefore - promotionDiscount
  }

  ////////// function calPromotions /////////////
  function calPromotions(promotionDiscount: number, totalBefore: number) {
    //// Check PromotionSelected
    for (const item of receiptPromotion.promotionSelected) {
      /// Check IsNotforOneItem?
      if (item.forOneItem === false) {
        // Check IsPercentDiscount?
        if (item.percentDiscount > 0) {
          // PercentDiscount
          promotionDiscount += (totalBefore * item.percentDiscount) / 100
        } else if (item.priceDiscount > 0) {
          // PriceDiscount
          promotionDiscount += item.priceDiscount
        }
      }

      /// Check IsforOneItem?
      if (
        item.forOneItem === true &&
        (item.minQty > 0 || item.minPrice > 0) &&
        item.productId >= 0
      ) {
        // console.log('Total before3: ', totalBefore)
        const receiptItemIndex = receiptItems.value.findIndex(
          (itemP) => itemP.product.id === item.productId
        )

        let qty = 0 // qty of product not qty sweetness
        for (let i = 0; i < receiptItems.value.length; i++) {
          if (receiptItems.value[i].productId === item.productId) {
            qty += receiptItems.value[i].qty
          }
        }

        if (receiptItemIndex !== -1) {
          const qtyDiscount = Math.floor(qty / item.minQty)
          if (item.percentDiscount > 0) {
            // console.log(receiptItems.value[receiptItemIndex].qty, item.minQty, qtyDiscount)
            // for loop for increase promotion by qty condition
            for (let i = 0; i < qtyDiscount; i++) {
              promotionDiscount +=
                ((receiptItems.value[receiptItemIndex].product.price * item.percentDiscount) /
                  100) *
                item.minQty
            }

            // console.log('qtyDiscount', qtyDiscount)
          } else if (item.priceDiscount > 0) {
            promotionDiscount += item.priceDiscount * qtyDiscount
          }
        }
      }

      if (memberStore.currentMember && item.member) {
        if (item.getPoint > 0) {
          gotPoint.value += item.getPoint
        }
        if (item.usePoint > 0) {
          usePoint.value += item.usePoint
        }
      }
    }
    return promotionDiscount
  }

  //////////////////////// Remove Items ////////////////////////////
  function removeReceioptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    receiptPromotion.clearPromotionSelect()
    calReceipt()
  }
  function increase(item: ReceiptItem) {
    item.qty++
    calReceipt()
  }
  function decrease(item: ReceiptItem) {
    receiptPromotion.clearPromotionSelect()
    if (item.qty === 1) {
      removeReceioptItem(item)
    }
    item.qty--
    calReceipt()
  }

  function showReceiptPayment() {
    if (receiptItems.value.length > 0) {
      receiptPayment.value = true
    }
  }

  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    // console.log(receipt.value.receiptItems)
    receiptDialog.value = true
  }

  //////////////////////// Clear Receipt ////////////////////////////
  async function clear() {
    cash.value = 0
    gotPoint.value = 0
    receiptDialog.value = false
    qrCode.value = false
    receiptItems.value = []
    receipt.value = {
      total: 0,
      qty: 0,
      created: new Date(),
      receiptItems: [],
      user: authStore.getCurrentUser(),
      cash: 0,
      discount: 0,
      member: null,
      payment: 'cash'
      // change: 0,
      // paymentType: 'cash',
      // memberDiscount: 0,
    }
    memberStore.clear()
    await receiptPromotion.clearPromotionSelect()
  }

  return {
    receiptItems,
    addReceiptItem,
    removeReceioptItem,
    increase,
    decrease,
    receipt,
    calReceipt,
    showReceiptDialog,
    receiptDialog,
    clear,
    showReceiptPayment,
    updateSweetness,
    receiptPayment,
    qrCode,
    cash,
    gotPoint,
    addReceiptBills
  }
})
