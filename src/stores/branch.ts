import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branchs = ref<Branch[]>([])
  const initialBranch: Branch = {
    name: '',
    address: ''
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initialBranch)))

  async function getBranch(id: number) {
    loadingStore.doLoading()
    const res = await branchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }
  async function getBranchs() {
    try {
      loadingStore.doLoading()
      const res = await branchService.getBranchs()
      branchs.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveBranch() {
    loadingStore.doLoading()
    const branch = editedBranch.value
    if (!branch.id) {
      // Add new
      console.log('Post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }

    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch() {
    loadingStore.doLoading()
    const branch = editedBranch.value
    const res = await branchService.delBranch(branch)

    await getBranchs()
    loadingStore.finish()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initialBranch))
  }
  return { branchs, getBranchs, saveBranch, deleteBranch, editedBranch, getBranch, clearForm }
})
