import { useLoadingStore } from '../loading'
import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import billPaymentService from '@/services/billPayment/billPayment'
import type { TypeBillPayment } from '@/types/BillPayment/TypeBillPayment'
import typeBillPaymentService from '@/services/billPayment/typeBillPayment'
import type { BillPayment } from '@/types/BillPayment/BillPayment'

export const useTypeBillPaymentStore = defineStore('typeBillPayment', () => {
  const loadingStore = useLoadingStore()
  const searchQuery = ref<string>('')
  const typeBillPayments = ref<TypeBillPayment[]>([])
  const initialTypeBillPayment: TypeBillPayment = {
    name: ''
  }

  const editedTypeBillPayment = ref<TypeBillPayment>(
    JSON.parse(JSON.stringify(initialTypeBillPayment))
  )

  // async function getTypeBillPayment(id: number) {
  //   try {
  //     loadingStore.doLoading()
  //     const res = await typeBillPaymentService.getTypeBillPayment(id)
  //     editedTypeBillPayment.value = res.data
  //     loadingStore.finish()
  //   } catch (e: any) {
  //     console.log('Error')
  //     loadingStore.finish()
  //   }
  // }

  async function getTypeBillPayments() {
    try {
      loadingStore.doLoading()
      const res = await billPaymentService.getBillPayments()
      typeBillPayments.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
    }
  }

  async function saveTypeBillPayment() {
    try {
      loadingStore.doLoading()
      const typeBillPayment = editedTypeBillPayment.value
      if (!typeBillPayment.id) {
        // Add new
        console.log('Post ' + JSON.stringify(typeBillPayment))
        const res = await typeBillPaymentService.addTypeBillPayment(typeBillPayment)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(typeBillPayment))
        const res = await typeBillPaymentService.updateTypeBillPayment(typeBillPayment)
      }
      await getTypeBillPayments()
      loadingStore.finish()
    } catch (e: any) {
      console.log('Error')
      loadingStore.finish()
    }
  }

  // async function checkAddUpdate(billPayment: BillPayment) {
  //   if (!billPayment.id) {
  //     // add new
  //     console.log('post => add ' + JSON.stringify(billPayment))
  //     const res = await billPaymentService.addBillPayment(billPayment)
  //   } else {
  //     // update
  //     console.log('patch => update ' + JSON.stringify(billPayment))
  //     const res = await billPaymentService.updateBillPayment(billPayment)
  //   }
  // }

  // const filteredBillPayments = computed(() => {
  //   const query = searchQuery.value.toLowerCase().trim()
  //   if (!query) {
  //     return billPayments.value // Return all bills if search query is empty
  //   } else {
  //     return billPayments.value.filter((billPayment) =>
  //     (billPayment.typeBill.name.toLowerCase().includes(query) ||
  //     billPayment.date.toString().toLowerCase().includes(query))
  //       // Add more fields to search as needed
  //     )
  //   }
  // })

  // async function saveTypeBillPayment() {
  //   try {
  //     loadingStore.doLoading()
  //     if (typeBill.value === true) {
  //       for (let i = 0; i < filteredBillPayments.value.length; i++) {
  //         checkAddUpdate(filteredBillPayments.value[i])
  //       }
  //     } else {
  //       const billPayment = editedBillPayment.value
  //       console.log('editedMaterial.value ' + JSON.stringify(editedBillPayment.value))
  //       checkAddUpdate(billPayment)
  //     }
  //     await getTypeBillPayments()
  //     loadingStore.finish()
  //   } catch (e: any) {
  //     //  messageStore.showMessage(e.message)
  //     console.log('Error')
  //     loadingStore.finish()
  //   }
  // }

  async function deleteTypeBillPayment() {
    loadingStore.doLoading()
    const typeBillPayment = editedTypeBillPayment.value
    const res = await typeBillPaymentService.delTypeBillPayment(typeBillPayment)

    await getTypeBillPayments()
    loadingStore.finish()
  }

  function clearForm() {
    editedTypeBillPayment.value = JSON.parse(JSON.stringify(initialTypeBillPayment))
  }
  return {
    typeBillPayments,
    getTypeBillPayments,
    saveTypeBillPayment,
    deleteTypeBillPayment,
    editedTypeBillPayment,
    searchQuery,
    clearForm
  }
})
