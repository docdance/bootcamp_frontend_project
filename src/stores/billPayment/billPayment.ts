import { useLoadingStore } from '../loading'
import { computed, ref } from 'vue'
import { defineStore } from 'pinia'
import billPaymentService from '@/services/billPayment/billPayment'
import type { BillPayment } from '@/types/BillPayment/BillPayment'
import { useMessageStore } from '../message'
import { useAuthStore } from '../auth'

export const useBillPaymentStore = defineStore('billPayment', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const billPayment = ref(false)
  const searchQuery = ref<string>('')
  const billPayments = ref<BillPayment[]>([])
  const initialBillPayment: BillPayment = {
    typeBill: 'Rent',
    total: 0,
    date: '',
    userId: authStore.getCurrentUser()?.id ?? null
    // time: ''
  }

  const editedBillPayment = ref<BillPayment>(JSON.parse(JSON.stringify(initialBillPayment)))

  async function getBillPayment(id: number) {
    try {
      loadingStore.doLoading()
      const res = await billPaymentService.getBillPayment(id)
      editedBillPayment.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function getBillPayments() {
    try {
      const res = await billPaymentService.getBillPayments()
      billPayments.value = res.data
      loadingStore.finish()
      console.log(res.data)
      console.log(billPayment.value)
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function getBillByUser(userId: number) {
    try {
      loadingStore.doLoading()
      const res = await billPaymentService.getBillPayment(userId)
      billPayment.value = res.data
      // loadingStore.success() //รอ loading new
      loadingStore.finish()
    } catch (e: any) {
      // loadingStore.fail()  //รอ loading new
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }

  async function checkAddUpdate(billPayment: BillPayment) {
    if (!billPayment.id) {
      // add new
      console.log('post => add ' + JSON.stringify(billPayment))
      const res = await billPaymentService.addBillPayment(billPayment)
    } else {
      // update
      console.log('patch => update ' + JSON.stringify(billPayment))
      const res = await billPaymentService.updateBillPayment(billPayment)
    }
  }

  const filteredBillPayments = computed(() => {
    const query = searchQuery.value.toLowerCase().trim()
    if (!query) {
      return billPayments.value // Return all bills if search query is empty
    } else {
      return billPayments.value.filter(
        (billPayment) =>
          billPayment.typeBill.toLowerCase().includes(query) ||
          billPayment.date.toString().toLowerCase().includes(query)
        // Add more fields to search as needed
      )
    }
  })

  // async function saveBillPayment() {
  //   try {
  //     loadingStore.doLoading()
  //     if (billPayment.value === true) {
  //       for (let i = 0; i < filteredBillPayments.value.length; i++) {
  //         checkAddUpdate(filteredBillPayments.value[i])
  //       }
  //     } else {
  //       const billPayment = editedBillPayment.value
  //       console.log('editedBillPayment.value ' + JSON.stringify(editedBillPayment.value))
  //       checkAddUpdate(billPayment)
  //     }
  //     await getBillPayments()
  //     loadingStore.finish()
  //   } catch (e: any) {
  //     //  messageStore.showMessage(e.message)
  //     console.log('Error')
  //     loadingStore.finish()
  //   }
  // }

  async function saveBillPayment() {
    try {
      loadingStore.doLoading()
      const billPayment = editedBillPayment.value
      console.log("bill1111111"+billPayment);
      
      if (!billPayment.id) {
        // Add new
        console.log('Post ' + JSON.stringify(billPayment))
        const res = await billPaymentService.addBillPayment(billPayment)
      } else {
   
        // Update
        console.log('Patch ' + JSON.stringify(billPayment))
        const res = await billPaymentService.updateBillPayment(billPayment)
      }
      await getBillPayments()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function deleteBillPayment() {
    loadingStore.doLoading()
    const billPayment = editedBillPayment.value
    const res = await billPaymentService.delBillPayment(billPayment)

    await getBillPayments()
    loadingStore.finish()
  }

  function clearForm() {
    editedBillPayment.value = JSON.parse(JSON.stringify(initialBillPayment))
  }

  return {
    billPayments,
    getBillPayments,
    getBillByUser,
    saveBillPayment,
    deleteBillPayment,
    editedBillPayment,
    getBillPayment,
    filteredBillPayments,
    searchQuery,
    checkAddUpdate,
    billPayment,
    clearForm
  }
})
