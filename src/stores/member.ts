import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'
import { useLoadingStore } from './loading'
import memberService from '@/services/member'
// import { useReceiptStore } from './receipt'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([])
  const isMember = ref(false)
  const loadingStore = useLoadingStore()
  const lastId = ref(5)
  const currentMember = ref<Member | null>()
  currentMember.value = null
  const showMember = ref(false)

  const searchMember = async (tel: string) => {
    try {
      loadingStore.doLoading()
      const res = await memberService.getMemberByTel(tel)
      currentMember.value = res.data
      loadingStore.finish()
      isMember.value = true
    } catch (e) {
      loadingStore.finish()
    }
  }

  function addMember(name: string, tel: string) {
    const newMember = ref<Member>({
      id: lastId.value++,
      name: name,
      tel: tel,
      point: 0,
      email: '',
      password: ''
    })
    memberService.addMember(newMember.value)
  }

  function getMembers() {
    memberService.getMembers()
  }

  function clear() {
    showMember.value = false
    isMember.value = false
    currentMember.value = null
  }
  return {
    members,
    currentMember,
    searchMember,
    clear,
    showMember,
    addMember,
    isMember,
    getMembers
  }
})
