type SalaryStatus = 'unpaid' | 'paid'
type Salary = {
  id?: number
  userId: number
  date: Date
  pay: number
  salaryStatus: SalaryStatus
}

export type { Salary, SalaryStatus }
