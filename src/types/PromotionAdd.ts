type PromotionAdd = {
  name: string
  description: string
  start_date: string
  end_date: string

  percentDiscount: number
  priceDiscount: number
  getPoint: number
  usePoint: number
  minQty: number
  minPrice: number
  member: boolean
  productId: number
  forOneItem: boolean
}

export type { PromotionAdd }
