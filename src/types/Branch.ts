export type Branch = {
  id?: number
  name: string
  address: string
}
