type Vender = {
  id?: number
  name: string
  tel: string
}

export { type Vender }
