import type { User } from './User'

interface Checkin {
  id: number
  user?: User
  date: Date
  checkintime: string // เวลาแบบ HH:MM:SS
  checkouttime: string // เวลาแบบ HH:MM:SS
  workkingtime: string // เวลาแบบ HH:MM:SS
}

export type { Checkin }
