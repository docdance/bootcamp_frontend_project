import type { User } from '../User'

type TypeBill = 'Rent' | 'Electricity' | 'Water' | 'Others'
type Billtotal = {
  totalAmount: number
}
type BillPayment = {
  id?: number
  date: string
  userId?: number | null
  user?: User
  typeBill: TypeBill
  total: number
  // time: string // datetime
}

export { type BillPayment, type TypeBill, type Billtotal }
