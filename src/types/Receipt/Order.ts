import type { Branch } from '../Branch'
import type { Member } from '../Member'
import type { Promotion } from '../Promotion'
import type { User } from '../User'
import type { OrderItems } from './OrderItems'

type Payment = 'cash' | 'mastercard' | 'qr-code'

type Order = {
  id?: number
  orderItemsIdQty: {
    productId: number
    qty: number
    sweet: string
  }[]
  created?: Date
  total: number
  qty: number
  userId?: number | null
  user?: User
  cash: number
  discount: number
  promotions: Promotion[]
  member: Member | null
  payment: Payment
  getPoint: number
  usePoint: number
  orderItems?: OrderItems[]
  branch: Branch
}

export type { Order }
