import type { Product } from '../Product'

type ReceiptItem = {
  id: number
  name: string
  price: number
  qty: number
  sweet: string
  productId: number
  product: Product
}

export { type ReceiptItem }
