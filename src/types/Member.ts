type Member = {
  id: number
  name: string
  email: string
  password: string
  tel: string
  point: number
}

export type { Member }
