import type { PurOrder } from './PurOrder'
import type { Material } from './Materials'

type OrderDetails = {
  id: number

  name: string

  price: number

  quantity: number

  total: number

  created: Date

  updated: Date

  purOrder: PurOrder

  material: Material
}

export type { OrderDetails }
