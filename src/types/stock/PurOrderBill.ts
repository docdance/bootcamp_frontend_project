import type { PurBillDetail } from './PurOrderDetail'
import type { User } from '../User'
import type { Vender } from '../Vender'

type PurOrderBill = {
  id?: number
  date: string
  total: number
  quantity: number
  nameStore: string

  purBillDetails: PurBillDetail[]
  user: User | null
  vender: Vender | null
}

export { type PurOrderBill }
