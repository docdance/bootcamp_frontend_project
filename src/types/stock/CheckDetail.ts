import type { Material } from './Materials'

type Status = 'Available' | 'Low'
type qt_previous = Material['quantity']

const defaultCheckDetail = {
  id: -1,
  in_date: '',
  name: '',
  price: 0,
  qt_previous: 0,
  quantity: 0,
  use: 0,
  min: 0,

  status: 'Low',
  materialId: -1,
  material: null
}

type CheckDetail = {
  id?: number
  in_date: string
  name: string
  price: number
  qt_previous: qt_previous
  quantity: number
  use: number
  min: number

  status: Status
  materialId?: number
  material?: Material | undefined
}

export { type CheckDetail, defaultCheckDetail }
