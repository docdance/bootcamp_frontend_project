import type { Material } from './Materials'

type PurBillDetail = {
  id: number
  name: string
  price: number
  quantity: number
  total: number
  materialId: number
  material: Material
}

export { type PurBillDetail }
