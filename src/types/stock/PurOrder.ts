import type { User } from '../User'
import type { Vender } from '../Vender'
import type { OrderDetails } from './OrderDetails'

type PurOrder = {
  id?: number
  purOrderDetails: {
    materialId: number
    quantity: number
  }[]
  date: string
  total: number
  quantity: number
  nameStore: string
  vender: Vender | null
  userId?: number | null
  user?: User
  orderDetails?: OrderDetails[]
  status: string
}

export type { PurOrder }
