import type { CheckDetail } from './CheckDetail'

import type { User } from '../User'

const defaultCheckMaterial = {
  id: -1,
  date: '',
  totalPrice: 0,
  totalQty: 0,
  totalUse: 0,
  employeeId: 1
}

type CheckMaterial = {
  id?: number
  date: string
  totalPrice: number
  totalQty: number
  totalUse: number
  userId: number
  userName: string
  stockDetails?: CheckDetail[]
  user?: User | null
}

export type { CheckMaterial, defaultCheckMaterial }
