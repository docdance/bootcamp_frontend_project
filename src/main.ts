// import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// Vuetify

import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import LongdoMap from 'longdo-map-vue'
const vuetify = createVuetify({
  icons: {
    defaultSet: 'mdi' // This is already the default value - only for display purposes
  },
  components,
  directives
})

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)
app.use(LongdoMap, {
  load: {
    apiKey: '8ff663cdff6e9d67558ffa3df6f63c14'
  }
})

app.mount('#app')
