import type { Branch } from '@/types/Branch'
import http from './http'

function addBranch(branch: Branch) {
  return http.post('/branchs', branch)
}

function updateBranch(branch: Branch) {
  return http.patch(`/branchs/${branch.id}`, branch)
}

function delBranch(branch: Branch) {
  return http.delete(`/branchs/${branch.id}`)
}

function getBranch(id: number) {
  return http.get(`/branchs/${id}`)
}

function getBranchs() {
  return http.get('/branchs')
}

function newBranch(name: string, address: string) {
  return http.get(`/branchs/inseartBranch${name}/${address}`)
}

export default { addBranch, updateBranch, delBranch, getBranch, getBranchs, newBranch }
