import http from './http'
import type { Checkin } from '@/types/checkin'

async function addCheckin(checkin: Checkin) {
  const res = await http.post('/checkins', checkin)
}

async function updateCheckin(checkin: Checkin) {
  const res = await http.patch('/checkins/' + checkin.id, checkin)
}

async function delCheckin(checkin: Checkin) {
  const res = await http.delete('/checkins/' + checkin.id)
}

function getCheckin(id: number) {
  return http.get('/checkins/' + id)
}

/////////// get latesOrder ///////////////////
function getLatesCheckin(id: number) {
  return http.get('/checkin/lates' + id)
}

function getOrders() {
  return http.get('/checkin')
}

function getCheckinAll() {
  return http.post(`/checkin/CheckinAll`)
}

export default {
  addCheckin,
  updateCheckin,
  delCheckin,
  getCheckin,
  getOrders,
  getCheckinAll,
  getLatesCheckin
}
