import type { BillPayment } from '@/types/BillPayment/BillPayment'
import http from '../http'

function addBillPayment(billPayment: BillPayment) {
  return http.post('/billPayments', billPayment)
}

function updateBillPayment(billPayment: BillPayment) {
  return http.patch(`/billPayments/${billPayment.id}`, billPayment)
}

function delBillPayment(billPayment: BillPayment) {
  return http.delete(`/billPayments/${billPayment.id}`)
}

function getBillPayment(id: number) {
  return http.get(`/billPayments/${id}`)
}

function getBillPaymentsByType(typeId: number) {
  return http.get('/billPayments/type/' + typeId)
}

function getBillPayments() {
  return http.get('/billPayments')
}

export default {
  addBillPayment,
  updateBillPayment,
  delBillPayment,
  getBillPayment,
  getBillPayments,
  getBillPaymentsByType
}
