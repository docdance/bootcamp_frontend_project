import http from '../http'
import type { TypeBillPayment } from '@/types/BillPayment/TypeBillPayment'

async function addTypeBillPayment(typeBillPayment: TypeBillPayment) {
  const res = await http.post('/typeBillPayments', typeBillPayment)
}

async function updateTypeBillPayment(typeBillPayment: TypeBillPayment) {
  const res = await http.patch('/typeBillPayments' + typeBillPayment.id, typeBillPayment)
}

async function delTypeBillPayment(typeBillPayment: TypeBillPayment) {
  const res = await http.delete('/typeBillPayments/' + typeBillPayment.id)
}

async function getTypeBillPayment(id: number) {
  const res = await http.get('/typeBillPayments/' + id)
}

async function getTypeBillPayments() {
  const res = await http.get('/typeBillPayments/')
}

export default {
  addTypeBillPayment,
  updateTypeBillPayment,
  delTypeBillPayment,
  getTypeBillPayment,
  getTypeBillPayments
}
