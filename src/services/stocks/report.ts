import http from '../http'

function getYear(year: number) {
  return http.get(`/stock-report/reportUsedYear/${year}`)
}

function getYears() {
  return http.get(`/stock-report/reportUsedYear`)
}

export default { getYear, getYears }
