import http from '../http'

import type { CheckMaterial } from '@/types/stock/CheckMaterials'
import type { CheckDetail } from '@/types/stock/CheckDetail'
import material from './material'

type StockDto = {
  stockDetails: {
    materialId: number
    quantity: number
  }[]
  userId: number
}

function addStock(stock: CheckMaterial, stockDetail: CheckDetail[]) {
  const stockDto: StockDto = {
    stockDetails: [],
    userId: 0
  }
  stockDto.userId = stock.userId
  stockDto.stockDetails = stockDetail.map((item) => {
    return {
      materialId: item.materialId ?? item.material?.id ?? 0,
      quantity: item.quantity
    }
  })
  return http.post('/checkStocks', stockDto)
}

function getStocks() {
  return http.get('/checkStocks')
}

function getStock(id: number) {
  return http.get(`/checkStocks/${id}`)
}

function delStock(stock: CheckMaterial) {
  return http.delete(`/checkStocks/${stock.id}`)
}
export default { addStock, getStocks, delStock, getStock }
