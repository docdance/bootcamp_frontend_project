import http from '@/services/http'
import type { PurOrder } from '@/types/stock/PurOrder'
import type { PurOrderBill } from '@/types/stock/PurOrderBill'

async function addPurOrder(purOrder: PurOrder) {
  console.log(purOrder)
  const res = await http.post('/materialOrders', purOrder)
  // return http.post('/materialOrders', purOrder)
}

async function delPurOrder(purOrder: PurOrder) {
  const res = await http.delete('/materialOrders/' + purOrder.id)

  // return http.delete(`/materialOrders/${purOrder.id}`)
}

async function updatePurOrder(purOrder: PurOrder) {
  const res = await http.patch('/materialOrders/' + purOrder.id, purOrder)
}

function getPurOrders() {
  return http.get('/materialOrders')
}

function getPurOrder(id: number) {
  return http.get(`/materialOrders/${id}`)
}

/////////// get latesOrder ///////////////////
function getLatesOrder(id: number) {
  return http.get('/orders/lates' + id)
}

export default { addPurOrder, getPurOrders, getPurOrder, delPurOrder, updatePurOrder }
