import http from './http'
import type { Order } from '@/types/Receipt/Order'

async function addOrder(order: Order) {
  const res = await http.post('/orders', order)
}

async function updateOrder(order: Order) {
  const res = await http.patch('/orders/' + order.id, order)
}

async function delOrder(order: Order) {
  const res = await http.delete('/orders/' + order.id)
}

function getOrder(id: number) {
  return http.get('/orders/' + id)
}

/////////// get latesOrder ///////////////////
function getLatesOrder(id: number) {
  return http.get('/orders/lates' + id)
}

function getOrders() {
  return http.get('/orders')
}

function getMemberOrders(id: number) {
  return http.get(`/orders/memberId/${id}`)
}

function getOrderAll() {
  return http.post(`/orders/OrderAll`)
}

export default {
  addOrder,
  updateOrder,
  delOrder,
  getOrder,
  getOrders,
  getOrderAll,
  getMemberOrders
}
