import type { Salary } from '@/types/Salary'
import http from './http'

function addSalary(salary: Salary) {
  return http.post('/salarys', salary)
}

function updateSalary(salary: Salary) {
  return http.patch(`/salarys/${salary.id}`, salary)
}

function delSalary(salary: Salary) {
  return http.delete(`/salarys/${salary.id}`)
}

function getSalary(id: number) {
  return http.get(`/salarys/${id}`)
}

function getSalarys() {
  return http.get('/salarys')
}

export default { addSalary, updateSalary, delSalary, getSalary, getSalarys }
