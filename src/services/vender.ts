import type { Vender } from '@/types/Vender'
import http from './http'

// function addVender(vender: Vender & { files: File[] }) {
//   const formData = new FormData()
//   formData.append('name', vender.name)
//   formData.append('tel', vender.tel)

//   if (vender.files && vender.files.length > 0) formData.append('file', vender.files[0])
//   return http.post('/venders', formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

async function addVender(vender: Vender) {
  const res = await http.post('/venders', vender)
}

// function updateVender(vender: Vender & { files: File[] }) {
//   const formData = new FormData()
//   formData.append('name', vender.name)
//   formData.append('tel', vender.tel)

//   if (vender.files && vender.files.length > 0) formData.append('file', vender.files[0])
//   return http.post(`/venders/${vender.id}`, formData, {
//     headers: {
//       'Content-Type': 'multipart/form-data'
//     }
//   })
// }

async function updateVender(vender: Vender) {
  const res = await http.patch('/venders/' + vender.id, vender)
}

function delVender(vender: Vender) {
  return http.delete(`/venders/${vender.id}`)
}

function getVenderByTel(tel: string) {
  return http.get('/venders/tel/' + tel)
}

function getVenderByName(name: string) {
  return http.get('/venders/name/' + name)
}

function getVender(id: number) {
  return http.get(`/venders/${id}`)
}

function getVenders() {
  return http.get('/venders')
}

export default {
  addVender,
  updateVender,
  delVender,
  getVender,
  getVenders,
  getVenderByTel,
  getVenderByName
}
