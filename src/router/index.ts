import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/login' // Show login page first
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue'),
      meta: {
        layout: 'FullLayout',
        requireAuth: false
      }
    },
    {
      path: '/pos',
      name: 'pos',
      components: {
        default: () => import('../views/pos/PosView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/home',
      name: 'home',
      component: {
        default: () => import('../views/HomeView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/stock',
      name: 'stock',
      components: {
        default: () => import('../views/stock/StockView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/report_stock',
      name: 'report_stock',
      components: {
        default: () => import('../views/stock/ReportStock.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/report_pur',
      name: 'report_pur',
      components: {
        default: () => import('../views/stock/Report/purchase/ReportPur.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/reportorder',
      name: 'reportorder',
      components: {
        default: () => import('../views/stock/Report/ReportOrder.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/pur_order',
      name: 'pur_order',
      components: {
        default: () => import('../views/stock/purchaseOrder/PurchaseView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/users',
      name: 'users',
      components: {
        default: () => import('../views/UserView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/branch',
      name: 'branch',
      components: {
        default: () => import('../views/BranchView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checkincheckout',
      name: 'checkincheckout',
      components: {
        default: () => import('../views/CheckInCheckOut.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/checkindetail',
      name: 'checkindetail',
      components: {
        default: () => import('../views/CheckinDetail.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/member',
      name: 'member',
      components: {
        default: () => import('../views/MemberReport.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/product',
      name: 'product',
      components: {
        default: () => import('../views/ProductView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/promotion',
      name: 'promotion',
      components: {
        default: () => import('../views/PromotionView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/order',
      name: 'order',
      components: {
        default: () => import('../views/order/OrderView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salaryowner',
      name: 'salaryowner',
      components: {
        default: () => import('../views/salary/SalaryOwner.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salarymanager',
      name: 'salarymanager',
      components: {
        default: () => import('../views/salary/SalaryManager.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salarymanagement',
      name: 'salarymanagement',
      components: {
        default: () => import('../views/salary/SalaryManagement.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/salaryuser',
      name: 'salaryuser',
      components: {
        default: () => import('../views/salary/SalaryUser.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    },
    {
      path: '/billpayment',
      name: 'billpayment',
      components: {
        default: () => import('../views/billPayment/BillPaymentView.vue'),
        menu: () => import('../components/MainMenu.vue'),
        header: () => import('../components/AppBar.vue')
      },
      meta: {
        layout: 'MainLayout',
        requireAuth: true
      }
    }
  ]
})

function isLogin() {
  const user = localStorage.getItem('user')
  const member = localStorage.getItem('member')
  if (user) {
    return true
  } else if (member) {
    return true
  }
  return false
}

router.beforeEach((to, from) => {
  if (to.meta.requireAuth && !isLogin()) {
    router.replace('/login')

    // return {
    //   path: '/login',
    //   query: { redirect: to.fullPath }
    // }
  }
})

export default router
